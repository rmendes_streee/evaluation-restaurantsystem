﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RestaurantSystem;
using RestaurantSystem.Controllers;

namespace RestaurantSystem.Tests.Controllers
{
    [TestClass]
    public class RestaurantSystemControllerTest
    {
        [TestMethod]
        public void RT001()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Sistema de Cadastro de Restaurantes", result.ViewBag.SystemName);
        }

        [TestMethod]
        public void RT002()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Index(string.Empty) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT003()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT004()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Index("Restaurante 2") as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT005()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT006()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT007()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);

            // Act
            result = controller.Index(string.Empty) as ViewResult;

            // Assert
            Assert.IsNotNull(result);

        }

        [TestMethod]
        public void RT008()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT009()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT010()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Edit(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT011()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Delete(1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);

            // Act
            result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT012()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Create(new Models.Base.Restaurant()
            {
                ID = 0,
                name = "RT012"
            }) as ViewResult;

            // Assert
            Assert.IsNull(result);

            // Act
            result = controller.Index("RT012") as ViewResult;
            var res = result.Model as IEnumerable<Models.Base.Restaurant>;

            // Assert
            Assert.IsNotNull(res);

            // Act
            foreach (var item in res)
                controller.DeleteConfirmed(item.ID);

            result = controller.Index("RT012") as ViewResult;
            res = result.Model as IEnumerable<Models.Base.Restaurant>;

            // Assert
            Assert.AreEqual(res.Count(), 0);
        }

        [TestMethod]
        public void RT013()
        {
            // Arrange
            RestaurantsController controller = new RestaurantsController();

            // Act
            ViewResult result = controller.Index(string.Empty) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public void RT014()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Create(new Models.Base.Menu()
            {
                ID = 0,
                foodName = "RT014",
                foodPrice = 99.99,
                RestaurantID = 1
            }) as ViewResult;

            // Assert
            Assert.IsNull(result);

            // Act
            result = controller.Index() as ViewResult;
            var res = result.Model as IEnumerable<Models.Base.Menu>;
            res = res.Where(p => p.foodName == "RT014");

            // Assert
            Assert.IsTrue(res.Count() > 0);

            // Act
            foreach (var item in res)
                controller.DeleteConfirmed(item.ID);

            result = controller.Index() as ViewResult;
            res = result.Model as IEnumerable<Models.Base.Menu>;
            res = res.Where(p => p.foodName == "RT014");

            // Assert
            Assert.IsTrue(res.Count() == 0);
        }

        [TestMethod]
        public void RT015()
        {
            // Arrange
            MenusController controller = new MenusController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
