﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Base
{
    public class Restaurant
    {
        public Restaurant() { }
        public Restaurant(Restaurant p)
        {
            this.ID = p.ID;
            this.name = p.name;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [Display(Name = "Restaurantes")]
        [Required(ErrorMessage = "O campo Nome é requerido.")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "O campo Nome não pode conter mais que 100 caracteres e no mínimo 3")]
        public string name { get; set; }
    }
}
