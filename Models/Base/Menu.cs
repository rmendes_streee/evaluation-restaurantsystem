﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Models.Base
{
    public class Menu
    {
        public Menu() { }
        public Menu(Menu p)
        {
            this.ID = p.ID;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ID { get; set; }

        [ForeignKey("Restaurant")]
        [Display(Name = "Restaurante")]
        [Required(ErrorMessage = "O campo Restaurante é requerido.")]
        public int RestaurantID { get; set; }

        // Navigation property
        public virtual Restaurant Restaurant { get; set; }

        [Display(Name = "Prato")]
        [Required(ErrorMessage = "O campo Prato é requerido.")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "O campo Prato não pode conter mais que 100 caracteres e no mínimo 3")]
        public string foodName { get; set; }

        [Display(Name = "Preço R$")]
        [Required(ErrorMessage = "O campo Preço é requerido.")]
        public double foodPrice { get; set; }
    }
}
