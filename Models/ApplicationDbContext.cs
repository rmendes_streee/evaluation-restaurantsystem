﻿using Models.Base;
using System.Data.Entity;

namespace DataAccess
{
    public class ApplicationDbContext : DbContext
    {
        #region BaseTables
        public DbSet<Restaurant> Restaurant { get; set; }
        public DbSet<Menu> Menu { get; set; }
        #endregion

        public ApplicationDbContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
        public object Schemas { get; private set; }

        
    }
}
