// <auto-generated />
namespace Models.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class createDataBase : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(createDataBase));
        
        string IMigrationMetadata.Id
        {
            get { return "201805222042345_createDataBase"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
