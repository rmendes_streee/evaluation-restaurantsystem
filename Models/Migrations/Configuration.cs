namespace Models.Migrations
{
    using Base;
    using DataAccess;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ApplicationDbContext context)
        {
            #region Seeds
            var restaurant = new List<Restaurant>
            {
                new Restaurant { ID = 1, name = "Restaurante 1"},
                new Restaurant { ID = 2, name = "Restaurante 2"},
                new Restaurant { ID = 3, name = "Restaurante 3"}
            };
            restaurant.ForEach(s => context.Restaurant.AddOrUpdate(p => p.ID, s));
            context.SaveChanges();

            var menu = new List<Menu>
            {
                new Menu { ID = 1, RestaurantID = 1, foodName = "Macarr�o", foodPrice = 20 },
                new Menu { ID = 2, RestaurantID = 1, foodName = "Lasanha", foodPrice = 28 },
                new Menu { ID = 3, RestaurantID = 2, foodName = "Peixe", foodPrice = 20 },
                new Menu { ID = 4, RestaurantID = 2, foodName = "Arroz", foodPrice = 10 }
            };
            menu.ForEach(s => context.Menu.AddOrUpdate(p => p.ID, s));
            context.SaveChanges();

            #endregion
        }
    }
}
